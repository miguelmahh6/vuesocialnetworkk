import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { initializeApp } from "firebase/app";
import { getFirestore, onSnapshot } from "firebase/firestore";
import { getStorage,ref  } from "firebase/storage";


const firebaseConfig = {
    apiKey: "AIzaSyCmflSZNaWCkpR1bn36q5zkeI2W4b5jhjY",
    authDomain: "tpbd-80184.firebaseapp.com",
    databaseURL: "https://tpbd-80184-default-rtdb.firebaseio.com",
    projectId: "tpbd-80184",
    storageBucket: "tpbd-80184.appspot.com",
    messagingSenderId: "99260179955",
    appId: "1:99260179955:web:1b2119577b7d8e12a6de86"
};


const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const storage = getStorage(app);
const storageRef = ref(storage);


export { db, onSnapshot, storage ,storageRef};


createApp(App).use(store).use(router).mount('#app')
