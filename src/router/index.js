import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import results from '../views/results';
import profile from '../components/profile'
import registro from '../views/registro.vue';
import login from '../views/login';
import store from '@/store';

const routes = [
  { path: '/', name: 'login', component: login },
  { path: '/:pathMatch(.*)*', component: login },
  { path: '/results/:searched', name: 'results', component: results, meta: { requiresAuth: true } },
  { path: '/profile/:id', name: 'profile', component: profile, meta: { requiresAuth: true } },
  { path: '/registro', name: 'registro', component: registro },
  { path: '/home', name: 'home', component: HomeView, meta: { requiresAuth: true } }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL), routes
});
router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requiresAuth)) {
    if (store.state.userInSession == null) {
      next("/");
    } else {
      next();
    }
  } else {
    if (store.state.userInSession != null && (router.currentRoute.value.path == "/" || router.currentRoute.value.path == "/registro")) {
      next('/home');

    } else {
      next();
    }
  }
})

export default router
