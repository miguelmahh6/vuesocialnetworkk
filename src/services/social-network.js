import axios from 'axios'
import { collection, getDocs, updateDoc, doc, addDoc } from "firebase/firestore";
import { db, onSnapshot } from "../main";
import store from '../store/index'

import {
    getStorage,
    ref,
    uploadString,
} from "firebase/storage";
export default class socialNetworkService {

    static url = 'https://tpbd-80184-default-rtdb.firebaseio.com/';

    /*  static async test(asd) {
         const querySnapshot = await getDocs(collection(db, "publications"));
         querySnapshot.forEach((doc) => {
             console.log(`${doc.id} => ${doc.data()}`);
         });
     } */

    static async create(data) {
        try {
            return await addDoc(collection(db, 'publications'), data);

        } catch (error) {
            return console.log("error creating : " + error);
        }
    }

    static async getPublications(from, idUser) {

        await onSnapshot(collection(db, "publications"), (querySnapshot) => {
            var publications = [];
            querySnapshot.forEach((doc) => {
                let publication = doc.data();
                if (from == "home") {
                    if ((publication.idUser != store.getters["userInSession"].systemId) && (publication.emailUser != store.getters["userInSession"].email)) {
                        publication.documentId = doc.id;
                        publications.push(publication);
                    }
                } else if (from == "profile") {
                    if (publication.idUser == idUser) {
                        publication.documentId = doc.id;
                        publications.push(publication);
                    }
                }
                console.log(publications);
            });
            store.commit("setPublications", publications)
            // return publications
        });
    }

    static async update(idDocument, data) {
        try {

            return await updateDoc(doc(db, "publications", idDocument), data);

            //return await collection(db, "publications").doc(idDocument).set(data);
        } catch (error) {
            return console.log("error updating : " + error);

        }
    }

    static async searchPeople(nameSearched) {

        var peopleFound = [];
        const response = await axios.get(this.url + 'people.json');

        if (nameSearched == "all") {
            peopleFound = response.data;
        } else {
            response.data.forEach((element) => {
                var nameE =
                    element.firstName +
                    element.secondName +
                    element.firstLastName +
                    element.secondLastName;
                nameE = nameE.toLowerCase().trim();
                var nameA = nameSearched.toLowerCase().trim();
                if (nameE.indexOf(nameA) >= 0) {
                    peopleFound.push(element);
                }
            });
        }

        return peopleFound

    }

    static async editUser(index, modifiedUser) {
        var url = this.url + "people/" + index + ".json";
        await axios.put(url, modifiedUser);
    }

    static async newUser(request) {
        await axios.put(`${this.url + "people.json"}`, request);
        //var headers=this.headers;
        //  return this.http.post(`${this.url}`, request, { headers });
        //return this.http.post(`${this.url}`, request);

    }

    static uploadFile(imgBase64) {

        const storage = getStorage();

        const storageRef = ref(
            storage,
            "profileImages/profileImg_" + Date.now()
        );

        return uploadString(storageRef, imgBase64, "data_url")
    }


}