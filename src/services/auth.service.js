import { getAuth, signOut, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";
export default class authService {

    static  register(email, password) {
        return createUserWithEmailAndPassword(getAuth(), email, password)
    }


    static login(email, password) {
        const auth = getAuth();
        return signInWithEmailAndPassword(auth, email, password);
    }


    static signout() {
        const auth = getAuth();
        signOut(auth).then(() => {
            alert('¡Sesión cerrada! Inicia sesión.');
        }).catch((error) => {
        });
    }


}