import { createStore } from 'vuex'
import VuexPersistence from 'vuex-persist'

export default createStore({
  state: {
    userInSession: null,
    publications: [],
    isLoading: false
  },
  getters: {
    isLoading(state) { return state.isLoading },
    userInSession(state) { return state.userInSession },
    publications(state) { return state.publications }

  },
  mutations: {
    setUserInSession(state, userInSession) { state.userInSession = userInSession },
    setPublications(state, publications) { state.publications = publications },
    setIsLoading(state, isLoading) { state.isLoading = isLoading }


  },
  actions: {
  },
  modules: {
  },
  plugins: [
    new VuexPersistence({
      storage: window.localStorage
    }).plugin
  ]
})
